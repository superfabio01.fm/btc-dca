# Use the official Node.js 21.5.0 image as base
FROM node:21.5.0

# Set the working directory
WORKDIR /usr/src/app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy local code to the container image
COPY . .

# Build the app
RUN npm run build

# Expose the app on port 3000
EXPOSE 3000

# Start the app
CMD [ "npm", "run", "start:prod" ]
