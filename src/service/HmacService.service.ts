import { Injectable } from '@nestjs/common';
import { createHmac } from 'crypto';

@Injectable()
export class HmacService {
  generateSignature(secret: string, nonce: string): string {
    return createHmac('sha256', secret)
      .update(nonce.toString())
      .digest('base64');
  }
}
