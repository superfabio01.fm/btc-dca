/* eslint-disable @typescript-eslint/no-unused-vars */
import { Injectable, Logger } from '@nestjs/common';
import { firstValueFrom } from 'rxjs';
import { HmacService } from './service/HmacService.service';
import { HttpService } from '@nestjs/axios';
import { Cron } from '@nestjs/schedule';
import { GetBtcPriceResult } from './types/client';
import Client from 'nexo-pro';
@Injectable()
export class NexoService {
  constructor(
    private hmacService: HmacService,
    private httpService: HttpService,
  ) {}

  @Cron('1 * * * * *')
  async buyBtcEveryDay(): Promise<any> {
    try {
      const client = Client({
        api_key: process.env.NEXO_API_KEY,
        api_secret: process.env.NEXO_API_SECRET_KEY,
      });
      // infos
      const res = await client.getQuote({
        pair: 'BTC/USDT',
        amount: 1,
        side: 'buy',
      });
      const quantity = parseFloat(process.env.BTC_DCA_EUR_AMOUNT) / res.price;
      const buyBtc = await client.placeOrder({
        pair: 'BTC/USDT',
        quantity: quantity,
        side: 'buy',
        type: 'market',
      });
      console.log(`bought ${quantity} BTC for price: ${res.price}`);
    } catch (error) {
      console.error(
        'An error occurred while buying BTC:',
        error + ' check nexo pro balance',
      );
    }
  }
}
