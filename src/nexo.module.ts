import { Module } from '@nestjs/common';
import { AppController } from './nexo.controller';
import { NexoService } from './nexo.service';
import { ScheduleModule } from '@nestjs/schedule';
import { HttpModule } from '@nestjs/axios';
import { HmacService } from './service/HmacService.service';

@Module({
  imports: [HttpModule, ScheduleModule.forRoot()],
  controllers: [AppController],
  providers: [NexoService, HmacService],
})
export class AppModule {}
