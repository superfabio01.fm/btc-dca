import { Controller } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { HmacService } from './service/HmacService.service';

@Controller()
export class AppController {
  constructor(
    private httpService: HttpService,
    private hmacService: HmacService,
  ) {}
}
