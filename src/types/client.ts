export type NexoProClientOptions = {
  api_key: string;
  api_secret: string;
};
export type GetBtcPriceResult = {
  amount: string;
  pair: string;
  price: string;
  timestamp: string;
};
